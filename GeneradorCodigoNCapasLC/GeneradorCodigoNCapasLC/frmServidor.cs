﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utils;

namespace GeneradorCodigoNCapasLC
{
    public partial class frmServidor : Form
    {
       
        private Helper helper;       
        public frmServidor()
        {
            InitializeComponent();
        }

        private void frmServidor_Load(object sender, EventArgs e)
        {
            helper = new Helper();
            comboBoxAutenticacion.SelectedIndex = 0;
            DesabilitarLogin();
            
            textBoxServidor.Text = helper.buscarInstancia();
            textBoxServidor.Text = "LLC\\SQLEXPRESS";
        }

        private void HabilitarLogin()
        {
            lblUsuario.Enabled = true;
            lblPassword.Enabled = true;
            textBoxPassword.Enabled = true;
            textBoxUsuario.Enabled = true;
            textBoxUsuario.Focus();
        }

        private void DesabilitarLogin()
        {
            lblUsuario.Enabled = false;
            lblPassword.Enabled = false;
            textBoxPassword.Enabled = false;
            textBoxUsuario.Enabled = false;
        }

        private void buttonConectar_Click(object sender, EventArgs e)
        {
            if (textBoxUsuario.Text == string.Empty && textBoxPassword.Text == string.Empty & comboBoxAutenticacion.SelectedIndex == 0)
            {
                helper.Conexion(textBoxServidor.Text);                
            }
            else
            {
                if (comboBoxAutenticacion.SelectedIndex == 1)
                {
                    errorProvider1.Clear();
                    if (string.IsNullOrEmpty(textBoxUsuario.Text))
                    {
                        errorProvider1.SetError(textBoxUsuario, "Debe de especificar un usuario");
                        return;
                    }
                    if (string.IsNullOrEmpty(textBoxPassword.Text))
                    {
                        errorProvider1.SetError(textBoxPassword, "Debe de especificar un usuario");
                        return;
                    }
                    helper.Conexion(textBoxServidor.Text, textBoxUsuario.Text, textBoxPassword.Text);                    
                }

            }

            if (helper.DB() != null)
            {
                comboBoxBaseDatos.DataSource = helper.DB();
                buttonConfigurar.Enabled = true;
                comboBoxBaseDatos.Enabled = true;
            }
            else
            {
                comboBoxBaseDatos.Items.Add("No hay bases de datos en el servidor");
            }

        }

        private void buttonConfigurar_Click(object sender, EventArgs e)
        {
            frmConfiguracion frm = new frmConfiguracion();
            frm.lblServidor.Text = textBoxServidor.Text.Trim();
            frm.lblBaseDatos.Text = comboBoxBaseDatos.SelectedItem.ToString();
            frm.user = textBoxUsuario.Text.Trim();
            frm.pass = textBoxPassword.Text.Trim();
            frm.Show();
            this.Hide();
        }
    }
}
