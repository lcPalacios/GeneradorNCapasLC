﻿namespace GeneradorCodigoNCapasLC
{
    partial class frmConfiguracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBoxTodos = new System.Windows.Forms.CheckBox();
            this.txtEntities = new System.Windows.Forms.TextBox();
            this.txtBLL = new System.Windows.Forms.TextBox();
            this.txtDAL = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBoxProcedure = new System.Windows.Forms.CheckBox();
            this.checkBoxBLL = new System.Windows.Forms.CheckBox();
            this.checkBoxDAL = new System.Windows.Forms.CheckBox();
            this.checkBoxEntities = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxSolucion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRuta = new System.Windows.Forms.TextBox();
            this.btnCarpeta = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblBaseDatos = new System.Windows.Forms.Label();
            this.lblServidor = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewTablas = new System.Windows.Forms.DataGridView();
            this.Seleccionar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxObtenerPorId = new System.Windows.Forms.CheckBox();
            this.checkBoxObtener = new System.Windows.Forms.CheckBox();
            this.checkBoxEliminar = new System.Windows.Forms.CheckBox();
            this.checkBoxInsertar = new System.Windows.Forms.CheckBox();
            this.checkBoxModificar = new System.Windows.Forms.CheckBox();
            this.checkBoxMetodos = new System.Windows.Forms.CheckBox();
            this.buttonAtras = new System.Windows.Forms.Button();
            this.buttonGenerar = new System.Windows.Forms.Button();
            this.checkBoxTodasTablas = new System.Windows.Forms.CheckBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTablas)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.checkBoxTodos);
            this.panel3.Controls.Add(this.txtEntities);
            this.panel3.Controls.Add(this.txtBLL);
            this.panel3.Controls.Add(this.txtDAL);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.checkBoxProcedure);
            this.panel3.Controls.Add(this.checkBoxBLL);
            this.panel3.Controls.Add(this.checkBoxDAL);
            this.panel3.Controls.Add(this.checkBoxEntities);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Location = new System.Drawing.Point(389, 37);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(276, 162);
            this.panel3.TabIndex = 4;
            // 
            // checkBoxTodos
            // 
            this.checkBoxTodos.AutoSize = true;
            this.checkBoxTodos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxTodos.Location = new System.Drawing.Point(8, 28);
            this.checkBoxTodos.Name = "checkBoxTodos";
            this.checkBoxTodos.Size = new System.Drawing.Size(61, 17);
            this.checkBoxTodos.TabIndex = 14;
            this.checkBoxTodos.Text = "Todos";
            this.checkBoxTodos.UseVisualStyleBackColor = true;
            this.checkBoxTodos.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txtEntities
            // 
            this.txtEntities.Location = new System.Drawing.Point(149, 49);
            this.txtEntities.Name = "txtEntities";
            this.txtEntities.ReadOnly = true;
            this.txtEntities.Size = new System.Drawing.Size(120, 20);
            this.txtEntities.TabIndex = 27;
            this.txtEntities.Text = "Entities";
            // 
            // txtBLL
            // 
            this.txtBLL.Location = new System.Drawing.Point(149, 101);
            this.txtBLL.Name = "txtBLL";
            this.txtBLL.ReadOnly = true;
            this.txtBLL.Size = new System.Drawing.Size(120, 20);
            this.txtBLL.TabIndex = 29;
            this.txtBLL.Text = "Bussiness";
            // 
            // txtDAL
            // 
            this.txtDAL.Location = new System.Drawing.Point(149, 75);
            this.txtDAL.Name = "txtDAL";
            this.txtDAL.ReadOnly = true;
            this.txtDAL.Size = new System.Drawing.Size(120, 20);
            this.txtDAL.TabIndex = 28;
            this.txtDAL.Text = "DataAccess";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(154, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Codigo a generar";
            // 
            // checkBoxProcedure
            // 
            this.checkBoxProcedure.AutoSize = true;
            this.checkBoxProcedure.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxProcedure.Location = new System.Drawing.Point(8, 128);
            this.checkBoxProcedure.Name = "checkBoxProcedure";
            this.checkBoxProcedure.Size = new System.Drawing.Size(135, 17);
            this.checkBoxProcedure.TabIndex = 16;
            this.checkBoxProcedure.Text = "Proc. Almacenados";
            this.checkBoxProcedure.UseVisualStyleBackColor = true;
            // 
            // checkBoxBLL
            // 
            this.checkBoxBLL.AutoSize = true;
            this.checkBoxBLL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBLL.Location = new System.Drawing.Point(8, 102);
            this.checkBoxBLL.Name = "checkBoxBLL";
            this.checkBoxBLL.Size = new System.Drawing.Size(133, 17);
            this.checkBoxBLL.TabIndex = 15;
            this.checkBoxBLL.Text = "Logica de Negocio";
            this.checkBoxBLL.UseVisualStyleBackColor = true;
            this.checkBoxBLL.CheckedChanged += new System.EventHandler(this.checkBoxBLL_CheckedChanged);
            // 
            // checkBoxDAL
            // 
            this.checkBoxDAL.AutoSize = true;
            this.checkBoxDAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxDAL.Location = new System.Drawing.Point(8, 76);
            this.checkBoxDAL.Name = "checkBoxDAL";
            this.checkBoxDAL.Size = new System.Drawing.Size(116, 17);
            this.checkBoxDAL.TabIndex = 14;
            this.checkBoxDAL.Text = "Acceso a Datos";
            this.checkBoxDAL.UseVisualStyleBackColor = true;
            this.checkBoxDAL.CheckedChanged += new System.EventHandler(this.checkBoxDAL_CheckedChanged);
            // 
            // checkBoxEntities
            // 
            this.checkBoxEntities.AutoSize = true;
            this.checkBoxEntities.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxEntities.Location = new System.Drawing.Point(8, 50);
            this.checkBoxEntities.Name = "checkBoxEntities";
            this.checkBoxEntities.Size = new System.Drawing.Size(82, 17);
            this.checkBoxEntities.TabIndex = 13;
            this.checkBoxEntities.Text = "Entidades";
            this.checkBoxEntities.UseVisualStyleBackColor = true;
            this.checkBoxEntities.CheckedChanged += new System.EventHandler(this.checkBoxEntities_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Codigo a generar";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxSolucion);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtRuta);
            this.panel1.Controls.Add(this.btnCarpeta);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblBaseDatos);
            this.panel1.Controls.Add(this.lblServidor);
            this.panel1.Location = new System.Drawing.Point(12, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(356, 162);
            this.panel1.TabIndex = 5;
            // 
            // textBoxSolucion
            // 
            this.textBoxSolucion.Location = new System.Drawing.Point(10, 129);
            this.textBoxSolucion.Name = "textBoxSolucion";
            this.textBoxSolucion.Size = new System.Drawing.Size(195, 20);
            this.textBoxSolucion.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(198, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Espacio de nombre de la solucion";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Carpeta del proyecto";
            // 
            // txtRuta
            // 
            this.txtRuta.Location = new System.Drawing.Point(10, 78);
            this.txtRuta.Name = "txtRuta";
            this.txtRuta.ReadOnly = true;
            this.txtRuta.Size = new System.Drawing.Size(270, 20);
            this.txtRuta.TabIndex = 23;
            this.txtRuta.Text = "C:\\";
            // 
            // btnCarpeta
            // 
            this.btnCarpeta.Location = new System.Drawing.Point(286, 76);
            this.btnCarpeta.Name = "btnCarpeta";
            this.btnCarpeta.Size = new System.Drawing.Size(36, 23);
            this.btnCarpeta.TabIndex = 22;
            this.btnCarpeta.Text = "...";
            this.btnCarpeta.UseVisualStyleBackColor = true;
            this.btnCarpeta.Click += new System.EventHandler(this.btnCarpeta_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Base de Datos :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Servidor:";
            // 
            // lblBaseDatos
            // 
            this.lblBaseDatos.AutoSize = true;
            this.lblBaseDatos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBaseDatos.Location = new System.Drawing.Point(111, 37);
            this.lblBaseDatos.Name = "lblBaseDatos";
            this.lblBaseDatos.Size = new System.Drawing.Size(0, 13);
            this.lblBaseDatos.TabIndex = 10;
            // 
            // lblServidor
            // 
            this.lblServidor.AutoSize = true;
            this.lblServidor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServidor.Location = new System.Drawing.Point(111, 14);
            this.lblServidor.Name = "lblServidor";
            this.lblServidor.Size = new System.Drawing.Size(0, 13);
            this.lblServidor.TabIndex = 9;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewTablas);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 230);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(360, 249);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tablas";
            // 
            // dataGridViewTablas
            // 
            this.dataGridViewTablas.AllowUserToAddRows = false;
            this.dataGridViewTablas.AllowUserToDeleteRows = false;
            this.dataGridViewTablas.AllowUserToOrderColumns = true;
            this.dataGridViewTablas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTablas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccionar});
            this.dataGridViewTablas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTablas.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewTablas.Name = "dataGridViewTablas";
            this.dataGridViewTablas.ReadOnly = true;
            this.dataGridViewTablas.Size = new System.Drawing.Size(354, 230);
            this.dataGridViewTablas.TabIndex = 0;
            this.dataGridViewTablas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTablas_CellClick);
            // 
            // Seleccionar
            // 
            this.Seleccionar.Frozen = true;
            this.Seleccionar.HeaderText = "Seleccionar";
            this.Seleccionar.Name = "Seleccionar";
            this.Seleccionar.ReadOnly = true;
            this.Seleccionar.Width = 80;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxObtenerPorId);
            this.groupBox3.Controls.Add(this.checkBoxObtener);
            this.groupBox3.Controls.Add(this.checkBoxEliminar);
            this.groupBox3.Controls.Add(this.checkBoxInsertar);
            this.groupBox3.Controls.Add(this.checkBoxModificar);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(449, 230);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(161, 156);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Metodos";
            // 
            // checkBoxObtenerPorId
            // 
            this.checkBoxObtenerPorId.AutoSize = true;
            this.checkBoxObtenerPorId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxObtenerPorId.Location = new System.Drawing.Point(7, 126);
            this.checkBoxObtenerPorId.Name = "checkBoxObtenerPorId";
            this.checkBoxObtenerPorId.Size = new System.Drawing.Size(101, 17);
            this.checkBoxObtenerPorId.TabIndex = 22;
            this.checkBoxObtenerPorId.Text = "ObtenerPorId";
            this.checkBoxObtenerPorId.UseVisualStyleBackColor = true;
            // 
            // checkBoxObtener
            // 
            this.checkBoxObtener.AutoSize = true;
            this.checkBoxObtener.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxObtener.Location = new System.Drawing.Point(7, 103);
            this.checkBoxObtener.Name = "checkBoxObtener";
            this.checkBoxObtener.Size = new System.Drawing.Size(71, 17);
            this.checkBoxObtener.TabIndex = 21;
            this.checkBoxObtener.Text = "Obtener";
            this.checkBoxObtener.UseVisualStyleBackColor = true;
            // 
            // checkBoxEliminar
            // 
            this.checkBoxEliminar.AutoSize = true;
            this.checkBoxEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxEliminar.Location = new System.Drawing.Point(6, 80);
            this.checkBoxEliminar.Name = "checkBoxEliminar";
            this.checkBoxEliminar.Size = new System.Drawing.Size(70, 17);
            this.checkBoxEliminar.TabIndex = 20;
            this.checkBoxEliminar.Text = "Eliminar";
            this.checkBoxEliminar.UseVisualStyleBackColor = true;
            // 
            // checkBoxInsertar
            // 
            this.checkBoxInsertar.AutoSize = true;
            this.checkBoxInsertar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxInsertar.Location = new System.Drawing.Point(7, 34);
            this.checkBoxInsertar.Name = "checkBoxInsertar";
            this.checkBoxInsertar.Size = new System.Drawing.Size(69, 17);
            this.checkBoxInsertar.TabIndex = 17;
            this.checkBoxInsertar.Text = "Insertar";
            this.checkBoxInsertar.UseVisualStyleBackColor = true;
            // 
            // checkBoxModificar
            // 
            this.checkBoxModificar.AutoSize = true;
            this.checkBoxModificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxModificar.Location = new System.Drawing.Point(7, 57);
            this.checkBoxModificar.Name = "checkBoxModificar";
            this.checkBoxModificar.Size = new System.Drawing.Size(78, 17);
            this.checkBoxModificar.TabIndex = 19;
            this.checkBoxModificar.Text = "Modificar";
            this.checkBoxModificar.UseVisualStyleBackColor = true;
            // 
            // checkBoxMetodos
            // 
            this.checkBoxMetodos.AutoSize = true;
            this.checkBoxMetodos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMetodos.Location = new System.Drawing.Point(549, 217);
            this.checkBoxMetodos.Name = "checkBoxMetodos";
            this.checkBoxMetodos.Size = new System.Drawing.Size(61, 17);
            this.checkBoxMetodos.TabIndex = 18;
            this.checkBoxMetodos.Text = "Todos";
            this.checkBoxMetodos.UseVisualStyleBackColor = true;
            this.checkBoxMetodos.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
            // 
            // buttonAtras
            // 
            this.buttonAtras.BackColor = System.Drawing.Color.White;
            this.buttonAtras.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
            this.buttonAtras.FlatAppearance.BorderSize = 2;
            this.buttonAtras.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.buttonAtras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAtras.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAtras.Location = new System.Drawing.Point(390, 439);
            this.buttonAtras.Name = "buttonAtras";
            this.buttonAtras.Size = new System.Drawing.Size(113, 37);
            this.buttonAtras.TabIndex = 9;
            this.buttonAtras.Text = "&Atras";
            this.buttonAtras.UseVisualStyleBackColor = false;
            // 
            // buttonGenerar
            // 
            this.buttonGenerar.BackColor = System.Drawing.Color.White;
            this.buttonGenerar.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
            this.buttonGenerar.FlatAppearance.BorderSize = 2;
            this.buttonGenerar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.buttonGenerar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGenerar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGenerar.Location = new System.Drawing.Point(545, 439);
            this.buttonGenerar.Name = "buttonGenerar";
            this.buttonGenerar.Size = new System.Drawing.Size(113, 37);
            this.buttonGenerar.TabIndex = 10;
            this.buttonGenerar.Text = "&Generar";
            this.buttonGenerar.UseVisualStyleBackColor = false;
            this.buttonGenerar.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBoxTodasTablas
            // 
            this.checkBoxTodasTablas.AutoSize = true;
            this.checkBoxTodasTablas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxTodasTablas.Location = new System.Drawing.Point(268, 217);
            this.checkBoxTodasTablas.Name = "checkBoxTodasTablas";
            this.checkBoxTodasTablas.Size = new System.Drawing.Size(104, 17);
            this.checkBoxTodasTablas.TabIndex = 1;
            this.checkBoxTodasTablas.Text = "Marcar Todas";
            this.checkBoxTodasTablas.UseVisualStyleBackColor = true;
            this.checkBoxTodasTablas.CheckedChanged += new System.EventHandler(this.checkBoxTodasTablas_CheckedChanged);
            // 
            // frmConfiguracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(674, 488);
            this.Controls.Add(this.checkBoxTodasTablas);
            this.Controls.Add(this.buttonGenerar);
            this.Controls.Add(this.checkBoxMetodos);
            this.Controls.Add(this.buttonAtras);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Name = "frmConfiguracion";
            this.Text = "frmConfiguracion";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmConfiguracion_FormClosed);
            this.Load += new System.EventHandler(this.frmConfiguracion_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTablas)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtEntities;
        private System.Windows.Forms.TextBox txtBLL;
        private System.Windows.Forms.TextBox txtDAL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBoxProcedure;
        private System.Windows.Forms.CheckBox checkBoxBLL;
        private System.Windows.Forms.CheckBox checkBoxDAL;
        private System.Windows.Forms.CheckBox checkBoxEntities;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtRuta;
        private System.Windows.Forms.Button btnCarpeta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblBaseDatos;
        public System.Windows.Forms.Label lblServidor;
        private System.Windows.Forms.TextBox textBoxSolucion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxTodos;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewTablas;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBoxObtenerPorId;
        private System.Windows.Forms.CheckBox checkBoxObtener;
        private System.Windows.Forms.CheckBox checkBoxMetodos;
        private System.Windows.Forms.CheckBox checkBoxEliminar;
        private System.Windows.Forms.CheckBox checkBoxInsertar;
        private System.Windows.Forms.CheckBox checkBoxModificar;
        private System.Windows.Forms.Button buttonAtras;
        private System.Windows.Forms.Button buttonGenerar;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Seleccionar;
        private System.Windows.Forms.CheckBox checkBoxTodasTablas;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}