﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utils;

namespace GeneradorCodigoNCapasLC
{
    public partial class frmConfiguracion : Form
    {
        Helper helper;
        public string cadenaConexion;
        public string servidor;
        public string user;
        public string pass;
        private CheckBox[] chkMetodos;

        public frmConfiguracion()
        {
            InitializeComponent();
        }

        private void frmConfiguracion_Load(object sender, EventArgs e)
        {
            helper = new Helper();
            Tablas();
            txtRuta.Text = @"C:\Users\LC\Desktop\ardu";
            chkMetodos = new CheckBox[] {checkBoxInsertar, checkBoxModificar,
                checkBoxEliminar, checkBoxObtener, checkBoxObtenerPorId };
            textBoxSolucion.Text = lblBaseDatos.Text;
        }

        public void Tablas()
        {
            if (user == string.Empty && pass == string.Empty)
            {
                helper.ConexionDB(lblServidor.Text,db:lblBaseDatos.Text);
            }
            else
            {
                helper.ConexionDB(lblServidor.Text, user, pass, lblBaseDatos.Text);
            }
            
            dataGridViewTablas.DataSource = helper.ListadoTablas(lblBaseDatos.Text).Tables[0];            
        }
        

        private void checkBoxEntities_CheckedChanged(object sender, EventArgs e)
        {
            VerificarCheckBox(checkBoxEntities, txtEntities);
        }

        private void VerificarCheckBox(CheckBox chk,TextBox txt)
        {
            if(chk.Checked == true)
            {
                txt.ReadOnly = false;
            }    
            else
            {
                txt.ReadOnly = true;
            }       
        }

        private void checkBoxDAL_CheckedChanged(object sender, EventArgs e)
        {
            VerificarCheckBox(checkBoxDAL, txtDAL);
        }

        private void checkBoxBLL_CheckedChanged(object sender, EventArgs e)
        {
            VerificarCheckBox(checkBoxBLL, txtBLL);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxTodos.Checked == true)
            {
                checkBoxEntities.Checked = true;
                checkBoxDAL.Checked = true;
                checkBoxBLL.Checked = true;
                checkBoxProcedure.Checked = true;
            }
            else
            {
                checkBoxEntities.Checked = false;
                checkBoxDAL.Checked = false;
                checkBoxBLL.Checked = false;
                checkBoxProcedure.Checked = false;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBoxSolucion.Text =="" )
            {
                MessageBox.Show("Escriba el nombre de la solucion", "Informacion");
                return;
            }

            CrearCarpeta(0);

            foreach (DataGridViewRow row in dataGridViewTablas.Rows)
            {
                if (row.Cells[0].Value != null)
                {
                    if (bool.Parse(row.Cells[0].Value.ToString()) == true)
                    {
                        if(checkBoxEntities.Checked)
                        {
                            CrearCarpeta(1);
                            GenerarEntidades(row.Cells[1].Value.ToString(), CrearCarpeta(1));                            
                        }
                        if (checkBoxDAL.Checked)
                        {
                            CrearCarpeta(2);
                            GenerarDAL(row.Cells[1].Value.ToString(), CrearCarpeta(2));
                        }
                        if (checkBoxBLL.Checked)
                        {
                            CrearCarpeta(3);
                            GenerarBLL(row.Cells[1].Value.ToString(), CrearCarpeta(3));
                        }
                    }
                }
            }

            MessageBox.Show("Se crearon los archivos satisfactoriamente", "Generador", MessageBoxButtons.OK, MessageBoxIcon.Information); 
        }

        private string CrearCarpeta(int opcion)
        {
            string ubicacion = "";
            if(opcion ==0)
            {
                ubicacion = txtRuta.Text + "\\" + textBoxSolucion.Text;
            }
            if(opcion == 1)
            {
                ubicacion = txtRuta.Text + "\\" + textBoxSolucion.Text + "\\" + textBoxSolucion.Text+".Entities";
            }
            else if(opcion == 2)
            {
                ubicacion = txtRuta.Text + "\\" +textBoxSolucion.Text + "\\" + textBoxSolucion.Text + ".DataAccess";
            }
            else if (opcion == 3)
            {
                ubicacion = txtRuta.Text + "\\" + textBoxSolucion.Text + "\\" + textBoxSolucion.Text + ".Business";
            }
            else if (opcion == 4)
            {
                ubicacion = txtRuta.Text + "\\" +textBoxSolucion.Text+ "\\" + textBoxSolucion.Text + ".Procedure";
            }

            if (!Directory.Exists(ubicacion))
            {
                Directory.CreateDirectory(ubicacion);
            }
            return ubicacion;          
        }

        private void GenerarBLL(string clase, string ubicacion)
        {
            StreamWriter sw = new StreamWriter(ubicacion + "\\" + clase + "BLL.cs", false);

            sw.WriteLine("using System;");
            sw.WriteLine("using System.Collections.Generic;");            
            sw.WriteLine("using System.Data.SqlClient;");
            sw.WriteLine("using {0}.{1};", textBoxSolucion.Text, txtEntities.Text);
            sw.WriteLine("using {0}.{1};\n", textBoxSolucion.Text, txtDAL.Text);
            sw.WriteLine("namespace {0}.{1}", textBoxSolucion.Text, txtBLL.Text);
            sw.WriteLine("{");
            sw.WriteLine("\tpublic class {0}BLL:Conexion", clase);
            sw.WriteLine("\t{");

            sw.WriteLine($"\t\tprivate static {clase}BLL instance;");
            sw.WriteLine($"\t\tpublic static {clase}BLL Instance");
            sw.WriteLine("\t\t{");
            sw.WriteLine("\t\t\tget \n\t\t\t{ \n\t\t\t\tif(instance == null)");
            sw.WriteLine("\t\t\t\t\tinstance = new " + clase + "BLL();");
            sw.WriteLine("\t\t\t\treturn instance;\n\t\t\t}\n\t\t}\n");

            if (checkBoxInsertar.Checked)
            {               
                sw.WriteLine(string.Format($"\t\tpublic bool Insert({clase}Entity entity"));
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\tusing(SqlConnection conn = new SqlConnection(conexion))");
                sw.WriteLine("\t\t\t{");
                sw.WriteLine("\t\t\t\ttry");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tconn.Open();");
                sw.WriteLine($"\t\t\t\t\treturn {clase}.Instance.Insert(entity, conn);;");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t\tcatch (Exception)");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tthrow;");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t}");
                sw.WriteLine("\t\t}\n\n");
                
            }
            if (checkBoxModificar.Checked)
            {
                sw.WriteLine($"\t\tpublic bool Update({clase}Entity entity)");
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\tusing(SqlConnection conn = new SqlConnection(conexion))");
                sw.WriteLine("\t\t\t{");
                sw.WriteLine("\t\t\t\ttry");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tconn.Open();");
                sw.WriteLine($"\t\t\t\t\treturn {clase}.Instance.Update(entity, conn);");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t\tcatch (Exception)");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tthrow;");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t}");
                sw.WriteLine("\t\t}\n\n");
            }
            if (checkBoxEliminar.Checked)
            {
                sw.WriteLine("\t\tpublic bool Delete(int id)");
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\tusing(SqlConnection conn = new SqlConnection(conexion))");
                sw.WriteLine("\t\t\t{");
                sw.WriteLine("\t\t\t\ttry");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tconn.Open();");
                sw.WriteLine($"\t\t\t\t\treturn {clase}.Instance.Delete(id,conn);");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t\tcatch (Exception)");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tthrow;");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t}");
                sw.WriteLine("\t\t}\n\n");
            }
            if (checkBoxObtener.Checked)
            {
                sw.WriteLine($"\t\tpublic List<{clase}Entity> GetAll()");
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\tusing(SqlConnection conn = new SqlConnection(conexion))");
                sw.WriteLine("\t\t\t{");
                sw.WriteLine("\t\t\t\ttry");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tconn.Open();");
                sw.WriteLine($"\t\t\t\t\treturn {clase}.Instance.GetAll(conn);");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t\tcatch (Exception)");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tthrow;");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t}");
                sw.WriteLine("\t\t}\n\n");
            }
            if (checkBoxObtenerPorId.Checked)
            {
                sw.WriteLine($"\t\tpublic bool GetById(int id)");
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\tusing(SqlConnection conn = new SqlConnection(conexion))");
                sw.WriteLine("\t\t\t{");
                sw.WriteLine("\t\t\t\ttry");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tconn.Open();");
                sw.WriteLine($"\t\t\t\t\treturn {clase}.Instance.GetById(id,conn);;");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t\tcatch (Exception)");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tthrow;");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t}");
                sw.WriteLine("\t\t}");
            }

            sw.WriteLine("\t}");
            sw.WriteLine("}");

            sw.Close();
        }

        private void GenerarDAL(string clase, string ubicacion)
        {
            
            StreamWriter sw = new StreamWriter(ubicacion + "\\" + clase + "DAL.cs", false);

            sw.WriteLine("using System;");
            sw.WriteLine("using System.Collections.Generic;");
            sw.WriteLine("using System.Data;");
            sw.WriteLine("using System.Data.SqlClient;");
            sw.WriteLine("using {0}.{1};\n", textBoxSolucion.Text, txtEntities.Text);
            sw.WriteLine("namespace {0}.{1}", textBoxSolucion.Text, txtDAL.Text);
            sw.WriteLine("{");
            sw.WriteLine("\tpublic class {0}DAL", clase);
            sw.WriteLine("\t{");

            sw.WriteLine($"\t\tprivate static {clase}DAL instance;");
            sw.WriteLine($"\t\tpublic static {clase}DAL Instance");
            sw.WriteLine("\t\t{");
            sw.WriteLine("\t\t\tget \n\t\t\t{ \n\t\t\t\tif(instance == null)");
            sw.WriteLine("\t\t\t\t\tinstance = new " + clase + "DAL();");
            sw.WriteLine("\t\t\t\treturn instance;\n\t\t\t}\n\t\t}\n");
                        
            if (checkBoxInsertar.Checked)
            { 
                DataSet dsCol = helper.ObtenerColumnasTabla(clase);

                sw.WriteLine(string.Format($"\t\tpublic bool Insert({clase}Entity entity,SqlConnection conn)"));
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\tusing(SqlCommand cmd = new SqlCommand(\"sp" + clase + "Insert\", conn))");
                sw.WriteLine("\t\t\t{");
                foreach (DataRow row in dsCol.Tables[0].Rows)
                {
                    sw.WriteLine($"\t\t\t\tcmd.Parameters.AddWithValue(\"@" + row["column_name"].ToString() + "\", entity." + row["column_name"].ToString() + ");");
                }
                sw.WriteLine("\t\t\t\tcmd.CommandType = CommandType.StoredProcedure;");
                sw.WriteLine("\t\t\t\treturn cmd.ExecuteNonQuery() > 0;");
                sw.WriteLine("\t\t\t}");
                sw.WriteLine("\t\t}\n\n");               
            }
            if (checkBoxModificar.Checked)
            {
                DataSet dsCol = helper.ObtenerColumnasTabla(clase);

                sw.WriteLine(string.Format($"\t\tpublic bool Update({clase}Entity entity,SqlConnection conn)"));
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\tusing(SqlCommand cmd = new SqlCommand(\"sp" + clase + "Update\", conn))");
                sw.WriteLine("\t\t\t{");
                foreach (DataRow row in dsCol.Tables[0].Rows)
                {
                    sw.WriteLine($"\t\t\t\tcmd.Parameters.AddWithValue(\"@" + row["column_name"].ToString() + "\", entity." + row["column_name"].ToString() + ");");
                }
                
                sw.WriteLine("\t\t\t\tcmd.CommandType = CommandType.StoredProcedure;");
                sw.WriteLine("\t\t\t\treturn cmd.ExecuteNonQuery() > 0;");
                sw.WriteLine("\t\t\t}");
                sw.WriteLine("\t\t}\n\n");
            }
            if (checkBoxEliminar.Checked)
            {                
                sw.WriteLine("\t\tpublic bool Delete(int id,SqlConnection conn)");
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\tusing(SqlCommand cmd = new SqlCommand(\"sp" + clase + "Delete\", conn))");
                sw.WriteLine("\t\t\t{");
                sw.WriteLine("\t\t\t\tcmd.Parameters.AddWithValue(\"@" + helper.ObtenerLLave(clase) + "\", id);");
                sw.WriteLine("\t\t\t\tcmd.CommandType = CommandType.StoredProcedure;\n");
                sw.WriteLine("\t\t\t\treturn cmd.ExecuteNonQuery() > 0;");
                sw.WriteLine("\t\t\t}");
                sw.WriteLine("\t\t}\n\n");
            }
            if(checkBoxObtener.Checked)
            {
                DataSet dsCol = helper.ObtenerColumnasTabla(clase);
                sw.WriteLine($"\t\tpublic List<{clase}Entity> GetAll(SqlConnection conn)");
                sw.WriteLine("\t\t{");
                sw.WriteLine($"\t\t\tList<{clase}Entity> listado= new List<{clase}Entity>();\n");
                sw.WriteLine("\t\t\tusing(SqlCommand cmd = new SqlCommand(\"sp" + clase + "GetAll\", conn))");
                sw.WriteLine("\t\t\t{");
                sw.WriteLine("\t\t\t\tcmd.CommandType = CommandType.StoredProcedure;\n");
                sw.WriteLine("\t\t\t\tusing (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.SingleResult))");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tif(dr != null)");
                sw.WriteLine("\t\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\t\twhile (dr.Read())");
                sw.WriteLine("\t\t\t\t\t\t{");
                sw.WriteLine($"\t\t\t\t\t\t\t{clase}Entity _entity = new {clase}Entity();");

                int indice = 0;
                foreach (DataRow row in dsCol.Tables[0].Rows)
                {
                    sw.WriteLine("\t\t\t\t\t\t\t_entity." + row["column_name"].ToString() + "=" + helper.ConvertirTipo(row["data_type"].ToString(), indice));
                    indice++;
                }

                sw.WriteLine("\t\t\t\t\t\t\tlistado.Add(_entity);");
                sw.WriteLine("\t\t\t\t\t\t}");
                sw.WriteLine("\t\t\t\t\t}");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t}");
                sw.WriteLine("\t\t\treturn listado;");
                sw.WriteLine("\t\t}\n\n");
            }
            if (checkBoxObtenerPorId.Checked)
            {
                DataSet dsCol = helper.ObtenerColumnasTabla(clase);
                sw.WriteLine($"\t\tpublic {clase}Entity GetById(int id,SqlConnection conn)");
                sw.WriteLine("\t\t{");
                sw.WriteLine($"\t\t\t{clase}Entity _entity= new {clase}Entity();\n");
                sw.WriteLine("\t\t\tusing(SqlCommand cmd = new SqlCommand(\"sp" + clase + "GetById\", conn))");
                sw.WriteLine("\t\t\t{");
                sw.WriteLine("\t\t\t\tcmd.CommandType = CommandType.StoredProcedure;");
                sw.WriteLine("\t\t\t\tcmd.Parameters.AddWithValue(\"@" + helper.ObtenerLLave(clase) + "\", id);\n");
                sw.WriteLine("\t\t\t\tusing (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.SingleResult))");
                sw.WriteLine("\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\tif(dr != null)");
                sw.WriteLine("\t\t\t\t\t{");
                sw.WriteLine("\t\t\t\t\t\twhile (dr.Read())");
                sw.WriteLine("\t\t\t\t\t\t{");

                int indice = 0;
                foreach (DataRow row in dsCol.Tables[0].Rows)
                {
                    sw.WriteLine("\t\t\t\t\t\t\t_entity." + row["column_name"].ToString() + "=" + helper.ConvertirTipo(row["data_type"].ToString(), indice));
                    indice++;
                }
                
                sw.WriteLine("\t\t\t\t\t\t}");
                sw.WriteLine("\t\t\t\t\t}");
                sw.WriteLine("\t\t\t\t}");
                sw.WriteLine("\t\t\t}");
                sw.WriteLine("\t\t\treturn _entity;");
                sw.WriteLine("\t\t}\n\n");
            }

            sw.WriteLine("\t}");
            sw.WriteLine("}");

            sw.Close();
        }      

        private void GenerarEntidades(string clase, string ubicacion)
        {            
            StreamWriter sw = new StreamWriter(ubicacion + "\\" + clase + "Entity.cs", false);

            sw.WriteLine("using System;\n");
            sw.WriteLine("namespace {0}.{1}",textBoxSolucion.Text,txtEntities.Text);
            sw.WriteLine("{");
            sw.WriteLine("\tpublic class {0}Entity", clase);
            sw.WriteLine("\t{");

            DataSet dsCol = helper.ObtenerColumnasTabla(clase);

            foreach (DataRow row in dsCol.Tables[0].Rows)
            {
                sw.WriteLine("\t\tpublic "+ helper.ConvertirTipo(row["data_type"].ToString()) +" " + row["column_name"].ToString() + " {get;set;}");                
            }
            sw.WriteLine("\t}");
            sw.WriteLine("}");

            sw.Close();
        }

        private void checkBoxTodasTablas_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxTodasTablas.Checked)
            {
                foreach (DataGridViewRow row in dataGridViewTablas.Rows)
                {
                    row.Cells[0].Value = true;
                }
            }
            else
            {
                foreach (DataGridViewRow row in dataGridViewTablas.Rows)
                {
                    row.Cells[0].Value = false;
                }
            }
        }

        

        private void dataGridViewTablas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (dataGridViewTablas.Rows[e.RowIndex].Cells[0].Value == null)
                    dataGridViewTablas.Rows[e.RowIndex].Cells[0].Value = true;
                else if (bool.Parse(dataGridViewTablas.Rows[e.RowIndex].Cells[0].Value.ToString()) == true)
                    dataGridViewTablas.Rows[e.RowIndex].Cells[0].Value = false;
                else if (bool.Parse(dataGridViewTablas.Rows[e.RowIndex].Cells[0].Value.ToString()) == false)
                    dataGridViewTablas.Rows[e.RowIndex].Cells[0].Value = true;
            }
        }

        private void btnCarpeta_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtRuta.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void frmConfiguracion_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (checkBoxMetodos.Checked)
            {
                foreach (CheckBox item in chkMetodos)
                {
                    item.Checked = true;
                }
            }
            else
            {
                foreach (CheckBox item in chkMetodos)
                {
                    item.Checked = false;
                }
            }
        }
    }
}
