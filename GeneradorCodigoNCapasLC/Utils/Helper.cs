﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public class Helper
    {
        private SqlDataSourceEnumerator servidores;
        private DataTable srvs = new DataTable();
        private SqlConnection conn;
        DataTable dt = new DataTable();

        public string buscarInstancia()
        {
            string sb = null;
            try
            {
                servidores = SqlDataSourceEnumerator.Instance;
                srvs = servidores.GetDataSources();

                foreach (DataRow item in srvs.Rows)
                {
                    //sb= string.Format(item["ServerName"].ToString() + "\\" + item["InstanceName"].ToString());
                    sb= string.Format("{0}\\{1}",item["ServerName"].ToString(), item["InstanceName"].ToString());
                }
            }
            catch (Exception)
            {
                sb = "";
            }
            return sb;
        }

        public void Conexion(string srv, string user=null, string pass=null)
        {
            try
            {
                if(user != null && pass != null)
                {
                    conn = new SqlConnection(@"Data Source =" + srv + "; Initial Catalog=master;USER ID = " + user + "; PWD =" + pass);
                }
                else
                {
                    conn = new SqlConnection(@"Data Source =" + srv + "; Initial Catalog=master;Integrated Security = True");
                }                
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ConexionDB(string srv, string user = null, string pass = null, string db=null)
        {
            try
            {
                if (user != null && pass != null)
                {
                    conn = new SqlConnection($"Data Source ={srv}; Initial Catalog={db};USER ID = {user};PWD={pass}");
                }
                else
                {
                    conn = new SqlConnection($"Data Source ={srv};Initial Catalog={db};Integrated Security = True");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<string> DB()
        {
            SqlConnection con = conn;
            List<string> ListdadoBD = new List<string>();
            try
            {
                con.Open();
                dt = conn.GetSchema("Databases");

                foreach (DataRow item in dt.Rows)
                {
                    ListdadoBD.Add((string) item[0]);
                }
                con.Close();
            }
            catch (Exception)
            {                
            }
            return ListdadoBD;
        }

        public DataSet ListadoTablas(string db)
        {
            SqlConnection con = conn;
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                using (SqlCommand cmd = new SqlCommand("Select table_name as 'Nombre Tabla' From INFORMATION_SCHEMA.Tables where table_type='BASE TABLE'",conn))
                {
                    con.Open();
                    da.SelectCommand = cmd;                    
                    da.Fill(ds);
                    con.Close();
                }                                
            }
            catch (Exception)
            {

            }
            return ds;
        }

        public DataSet ObtenerColumnasTabla(string tabla)
        {
            SqlConnection con = conn;
            
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                using (SqlCommand cmd = new SqlCommand("Select COLUMN_NAME,DATA_TYPE From INFORMATION_SCHEMA.columns where table_name=@TableName",con))
                {
                    con.Open();
                    cmd.Parameters.Add("@TableName", SqlDbType.NVarChar, 50).Value = tabla;
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    con.Close();
                }                
            }            
            catch (Exception )
            {               
            }
            return ds;
        }

        public String ConvertirTipo(string tipo)
        {
            string convertido = "";

            switch (tipo)
            {
                case "nchar":
                    convertido = "string";
                    break;
                case "numeric":
                    convertido = "decimal";
                    break;
                case "nvarchar":
                    convertido = "string";
                    break;
                case "money":
                    convertido = "decimal";
                    break;
                case "text":
                    convertido = "string";
                    break;
                case "bit":
                    convertido = "bool";
                    break;
                case "char":
                    convertido = "string";
                    break;
                case "date":
                    convertido = "DateTime";
                    break;
                case "datetime":
                    convertido = "DateTime";
                    break;
                case "float":
                    convertido = "double";
                    break;
                case "int":
                    convertido = "int";
                    break;
                case "varchar":
                    convertido = "string";
                    break;
            }
            return convertido;
        }


        public string ConvertirTipo(string tipo, int pos)
        {
            string convertido = "";

            switch (tipo)
            {
                case "nchar":
                    convertido = "dr[" + pos + "].ToString();";
                    break;
                case "numeric":
                    convertido = "(decimal)dr[" + pos + "];";
                    break;
                case "nvarchar":
                    convertido = "dr[" + pos + "].ToString();";
                    break;
                case "money":
                    convertido = "(decimal)dr[" + pos + "];";
                    break;
                case "date":
                    convertido = "(datetime)dr[" + pos + "];";
                    break;
                case "float":
                    convertido = "(double)dr[" + pos + "];";
                    break;
                case "int":
                    convertido = "int";
                    convertido = "(int)dr[" + pos + "];";
                    break;
                case "varchar":
                    convertido = "dr[" + pos + "].ToString();";
                    break;
            }
            return convertido;
        }

        public string ObtenerLLave(string tabla)
        {
            string llave = "";
            SqlConnection con = conn;
            try
            {
                using (SqlCommand cmd = new SqlCommand(" Select b.column_name,a.data_type From INFORMATION_SCHEMA.columns as a, INFORMATION_SCHEMA.KEY_COLUMN_USAGE as b where a.table_name=@TableName and constraint_name='PK_" + tabla + "' and a.column_name=b.column_name",con))
                {
                    con.Open();

                    cmd.Parameters.Add("@TableName", SqlDbType.NVarChar, 50).Value = tabla;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        llave = dr.GetString(0);
                    }
                    con.Close();
                }                
            }           
            catch (Exception )
            {
                
            }

            return llave;
        }

    }
}
